package myserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class NetServer {
    private int port;
    private ServerSocket ssocket;
    private Events events;

    public NetServer(int port, TrafficGenerators tg, Events events) throws IOException {
        this.port = port;
        this.ssocket = new ServerSocket(port);
        this.events = events;
        Socket socket = null;
        try {
            while (true) {
                socket = ssocket.accept();
                try {
                    new ServerThread(socket, tg, events);
                } catch (IOException e) {
                    socket.close();
                }
            }
        } catch (SocketException e) {
            assert socket != null;
            socket.close();
        } finally {
            ssocket.close();
        }

    }

    public ServerSocket getServerSocket() {
        return ssocket;
    }


}
