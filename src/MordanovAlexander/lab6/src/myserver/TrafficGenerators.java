package myserver;

import java.io.*;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Random;

public class TrafficGenerators {

    final static int quotecount = 420;
    private String qlist;

    TrafficGenerators(String qlist) {
        this.qlist = qlist;
    }

    public String getTime() {
        LocalTime time = LocalTime.now();

        return time.toString();
    }

    public String getQuote() throws IOException {
        Random rnd = new Random();
        int r = rnd.nextInt(quotecount+1);
        InputStream in = getClass().getResourceAsStream(qlist);
        LineNumberReader br = new LineNumberReader(new InputStreamReader(in,"UTF-8"));
        while(br.getLineNumber()<r*3-2) {
            br.readLine();
        }
        return br.readLine() + " (" + br.readLine() + ")";
    }

    double getCurValue(String ValCode, double Value, LocalDate toDate) {
        int day = toDate.getDayOfMonth();
        int month = toDate.getMonthValue();
        int year = toDate.getYear();
        String date = (day<10?"0":"")+String.valueOf(day)+"."+(month<10?"0":"")+String.valueOf(month)+"."+String.valueOf(year);
        try {
            URL bank = new URL("http://www.cbr.ru/currency_base/D_print.aspx?date_req=" + date);
            BufferedReader br = new BufferedReader(new InputStreamReader(bank.openStream(), "UTF-8"));
            String line;
            while(!(br.readLine()).contains(ValCode));
            br.readLine();
            br.readLine();
            line = br.readLine();
            String result = line.substring(line.indexOf('>') + 1, line.indexOf('<', line.indexOf('>') + 1)).replace(',', '.');
            return Double.parseDouble(result)*Value;
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
            return -1;
        }
    }

}

