package myserver;

import java.io.IOException;

public class NetTasksServer {

    public static void main(String[] args) {

        final String qlist = "/quotes/qlist.txt";
        final String elist = "/events/events.json";

        if(args.length<1) {
            System.out.println("Arguments: [0] - server port");
            return;
        }
        System.out.println("Server started");
        System.out.println("Port: " + args[0]);

        try {
            new NetServer(Integer.parseInt(args[0]), new TrafficGenerators(qlist), new Events(elist));
        }
        catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

