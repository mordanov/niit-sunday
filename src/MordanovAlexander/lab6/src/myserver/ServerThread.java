package myserver;

import java.io.*;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

class ServerThread extends Thread {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private TrafficGenerators tg;
    private int clientID = 0;
    private Events e;

    public ServerThread(Socket s, TrafficGenerators tg, Events e) throws IOException {
        socket = s;
        this.tg = tg;
        this.e = e;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        start();
    }

    public void run() {
        final String rhead = "##reminds##";
        final String chead = "##getcurs##";
        final String thead = "##gettime##";
        final String qhead = "##getquot##";
        final String ihead = "##user_id##";
        final String separator = "&";

        String[] valval;
        try {
            while (true) {
                // взять из входящего буфера все, что есть и разобрать
                String str = in.readLine();
                int oldid = this.clientID;
                if (str != null) {
                    StringBuilder result = new StringBuilder();
                    switch (str.toLowerCase().substring(0, 7)) {
                        case "gettime":
                            result = new StringBuilder(thead + separator + tg.getTime());
                            break;
                        case "getquot":
                            result = new StringBuilder(qhead + separator + tg.getQuote());
                            break;
                        case "getcurs":
                            valval = str.split(":");
                            result = new StringBuilder(chead + separator + String.valueOf(tg.getCurValue(valval[2], Double.parseDouble(valval[1]), LocalDate.now().minusDays(1))));
                            break;
                        case "user_id":
                            valval = str.split(":");
                            this.clientID = Integer.parseInt(valval[1]);
                            outstr("Change Client ID to: " + this.clientID);
                            result = new StringBuilder(ihead + separator + this.clientID);
                            break;
                        case "reminds":
                            List<Item> items = e.getCurrentItems(clientID);
                            outstr("Reminder: " + items.size() + " items");
                            result = new StringBuilder();
                            for(Item i:items) {
                                outstr("Reminder (client id = " + clientID + "): " + i.desc);
                                e.setItemState(i);
                                result.append(rhead + separator).append(i.desc);
                            }
                            if(result.length()==0) {
                                result.append("-");
                            }
                    }
                    if(result.length()>0) {
                        out.println(String.valueOf(oldid) + separator + result);
                        outstr(str + " -> " + String.valueOf(oldid) + separator + result);
                    }
                }
            }
        } catch (IOException ex) {
            outstr(ex.getMessage());
        } finally {
            try {
                out.println("Сервер недоступен!");
                socket.close();
            } catch (IOException ex) {
                outstr(ex.getMessage());
            }
        }
    }

    private void outstr(String text) {
        Date ld = new Date();
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        System.out.println(df.format(ld) + ' ' + text);
    }

}

