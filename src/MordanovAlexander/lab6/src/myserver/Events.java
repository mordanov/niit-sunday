package myserver;

import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Events {

    private String evfile;

    private List<Item> Items = new ArrayList<Item>() {};

    Events(String evfile) {
        this.evfile = evfile;
        try {
            loadEvents();
        }
        catch(IOException | ParseException | java.text.ParseException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadEvents() throws IOException, ParseException, java.text.ParseException {
        InputStream fr = getClass().getResourceAsStream(evfile);
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fr.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }

        JSONParser parser = new JSONParser();
        JSONObject js = (JSONObject)parser.parse(result.toString("UTF-8"));
        JSONArray items = (JSONArray)js.get("events");
        for(Object i:items) {
            JSONObject k = (JSONObject)i;
            int id0 = Integer.parseInt(k.get("id").toString());
            String desc0 = k.get("desc").toString();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date date0 = format.parse(k.get("datetime").toString());

            Items.add(new Item(id0, desc0, date0));
        }

    }

    public List<Item> getCurrentItems(int ID) {
        List<Item> result = new ArrayList<Item>(){};
        Date d = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for(Item g:Items) {
            if((df.format(g.date).equals(df.format(d))) & (g.id == ID) && !g.sent) {
                result.add(g);
            }
        }
        return result;
    }

    public void setItemState(Item i) {
        if(Items.indexOf(i)>=0) {
            i.sent = true;
        }
    }

}

