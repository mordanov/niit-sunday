package myserver;

import java.util.Date;

public class Item {
    int id;
    String desc;
    Date date;
    boolean sent;

    Item(int id, String desc, Date date) {
        this.id = id;
        this.desc = desc;
        this.date = date;
        this.sent = false;
    }
}
