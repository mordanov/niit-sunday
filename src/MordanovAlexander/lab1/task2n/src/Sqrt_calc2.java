public class Sqrt_calc2 {

    public static void main(String[] args) {
        if(args.length<2) {
            System.out.println("Parameters: [1] - X, [2] - precision");
            return;
        }
        double val=Double.parseDouble(args[0]);
        double precision=Double.parseDouble(args[1]);
        double result = new Sqrt().calc(val, precision);
        System.out.println("Sqrt of "+val+"="+result);    }
}

class Sqrt
{
    double delta;
    double arg;

    public static double average(double x,double y) {
        return (x+y)/2.0;
    }

    private boolean good(double guess,double x) {
        return Math.abs(guess*guess-x)<delta;
    }
    private static double improve(double guess,double x) {
        return average(guess,x/guess);
    }

    private double iter(double guess, double x) {
        if(good(guess,x))
            return guess;
        else
            return iter(improve(guess,x),x);
    }
    public double calc(double arg, double precision) {
        this.arg=arg;
        this.delta=precision;
        return iter(1.0,arg);
    }
}
