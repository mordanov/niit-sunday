import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class DekanatDemo {

    public static void main(String[] args) {
        Dekanat dekanat;

        if(args.length<2) {
            System.out.println("Параметры: {1} - имя файла студентов, {2} - имя файл групп, {3} - имя файла для сохранения студентов");
            return;
        }

        // загрузка файла
        dekanat = new Dekanat(args[0], args[1]);
        final Random rand = new Random();

        System.out.println("--------Сценарий 1: создание студентов на основе данных из файла");
        // вывод списка студентов
        dekanat.getStudentList();
        System.out.println("--------Сценарий 2: создание групп на основе данных из файла");
        // вывести список групп и количество студентов в каждой
        dekanat.getGroupList();
        System.out.println("--------Сценарий 3: добавление случайных оценок студентам");
        // вывод средних баллов (нули) до добавления
        // скрипт добавления оценок
        for(Object o:dekanat.getStudents()) {
            Student s = (Student)o;
            s.setRandomMarks(rand.nextInt(5)); // добавим от нуля до 4 новых оценок
        }
        // вывод средних баллов после добавления
        dekanat.getGroupList();
        System.out.println("--------Сценарий 4: накопление статистики по успеваемости студентов и групп");
        // сохранить оценки в объект
        HashMap<String, Double> oldgroupmarks = new HashMap<String, Double>();
        for(Object og:dekanat.getGroups()) {
            Group g=(Group)og;
            oldgroupmarks.put(g.title, g.avgGroupMark());
        }
        // добавить новые оценки
        for(Object o:dekanat.getStudents()) {
            Student s = (Student)o;
            s.setRandomMarks(rand.nextInt(5)); // добавим от нуля до 4 новых оценок
        }
        // вызвать метод, подсчитывающий успеваемость по сравнению с прошлым расчетом
        dekanat.getStatistics(oldgroupmarks);
        dekanat.getGroupList();
        System.out.println("--------Сценарий 5: перевод студентов из группы в группу");
        // перевести трех студентов из группы в группу
        int moved=0;
        int movedmax=rand.nextInt(5);
        while(moved<movedmax) { // переведем от одного до 5 студентов в какие-нибудь группы
            Group gn=(Group)dekanat.getGroups().get(rand.nextInt(dekanat.getGroups().size())); // случайная группа
            Student s = (Student)dekanat.getStudents().get(rand.nextInt(dekanat.getStudents().size())); // найдем случайного студента
            // если группа не совпала - переводим!
            if(!s.getGroup().equals(gn)) {
                dekanat.moveToGroup(s, gn);
                moved++;
            }
        }
        // вывести список групп и количество студентов в каждой
        dekanat.getGroupList();
        System.out.println("--------Сценарий 6: отчисление студентов за неуспеваемость");
        // отчислить студентов, у которых средний балл меньше 3,2
        dekanat.removeStudents(3.2);
        // вывести список групп и количество студентов в каждой
        dekanat.getGroupList();
        System.out.println("--------Сценарий 7: сохранение обновленных данных в файлах");
        // сохранить файлы
        dekanat.saveStudents(args[2]);
        System.out.println("--------Сценарий 8: инициация выборов старост в группах");
        // для каждой группы определить случайного студента как старосту
        for(Object o:dekanat.getGroups()) {
            Group g=(Group)o;
            g.setRandomHead();
        }
        dekanat.getGroupList();
    }

}

class Student {
    private int ID; // идентификатор студента
    private String FIO; // ФИО
    private Group group; // группа
    private List<Integer> marks = new ArrayList<Integer>(); // список его оценок

    public Student(int ID, String FIO) {
        this.ID = ID;
        this.FIO = FIO;
    }

    public String getFIO() { // получить ФИО
        return FIO;
    }

    public int getID() { // получить ID
        return ID;
    }

    public void setGroup(Group group) { // назначение в группу
        this.group = group;
    }

    public void addMark(int mark) { // добавить новую оценку
        marks.add(mark);
    }

    public int markCount() { // количество оценок
        return marks.size();
    }

    public double getAvgMark() { // средний балл
        double sum = 0;
        if (marks.size() > 0) {
            for (int m : marks) {
                sum += m;
            }
            return sum / marks.size();
        } else return 0.0;
    }

    public void setRandomMarks(int num) { //добавить num случайных оценок (от 1 до 5)
        final Random rand = new Random();
        for (int i = 0; i < num; i++)
            marks.add(rand.nextInt(4) + 1);
    }

    public Group getGroup() { // получить группу
        return group;
    }

    public void moveToGroup(Group g) { // переместить студента в группу g
        String o=group.title;
        group=g;
        System.out.println("Студент " + FIO + " переведен из группы " + o + " в группу " + g.title);
    }

    public List<Integer> getMarks() { // возвратить массив оценок
        return marks;
    }
}

class Group {
    public String title; // название группы
    private List<Student> students = new ArrayList<Student>(); // массив студентов
    private Student head; // староста

    public Group(String title) { // добавление группы
        this.title = title;
    }

    public void addStudent(Student student) { // добавление студента в группу
        students.add(student);
    }

    public Student newHead(Student head) { // избрание старосты
        if(students.indexOf(head)>=0)
            this.head = head;
        else System.out.println("Избрание старосты невозможно - его нет в списке студентов!");
        return head;
    }

    public Student setRandomHead() { // избрать случайного студента старостой
        final Random rand = new Random();
        head = students.get(rand.nextInt(students.size()));
        return head;
    }

    public Student searchID(int ID) { // поиск по ID в группе
        for(Student st:students) {
            if(st.getID()==ID) {
                return st;
            }
        }
        return null;
    }

    public Student searchFIO(String FIO) { // поиск по ФИО в группе
        for(Student st:students) {
            if(st.getFIO().equals(FIO)) {
                return st;
            }
        }
        return null;
    }

    public int groupSize() { // размер группы
        return students.size();
    }

    public double avgGroupMark() { // средний балл всей группы
        if(students.size()>0) {
            double sum = 0;
            for (Student st : students) {
                sum += st.getAvgMark();
            }
            return sum / students.size();
        }
        else return 0.0;
    }

    public void removeStudent(Student student) { // удалить студента из группы
        NumberFormat formatter = new DecimalFormat("#0.00");
        if(head!=null) {
            if (head.equals(student)) {// проверка на старосту
                head = null;
                System.out.println("Староста группы " + title + " отчислен! Необходимо выбрать нового.");
                System.out.println("Староста выбран случайно: теперь это " + setRandomHead().getFIO());
            }
        }
        if(students.indexOf(student)>=0) {
            System.out.println("Студент " + student.getFIO() + " удален из группы " + title + "! Ср.балл: " + formatter.format(student.getAvgMark()));
            students.remove(student);
        }
        else System.out.println("Невозможно удалить студента " + student.getFIO() + " из группы - его нет в списке!");
    }

    public Student getHead() { // вернуть старосту :)
        return head;
    }
}

class Dekanat {
    private List<Student> students = new ArrayList<Student>() {};
    private List<Group> groups = new ArrayList<Group>() {};

    public Dekanat(String fn_students, String fn_groups) { // загрузим студентов и группы
        // грузим группы
        File fg = new File(fn_groups);
        try {
            FileReader fr = new FileReader(fg);
            JSONParser parser = new JSONParser();
            JSONObject js = (JSONObject)parser.parse(fr);
            JSONArray items = (JSONArray)js.get("groups");
            for(Object i:items) {
                groups.add(new Group(i.toString())); // добавим в массив
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Файл с группами не найден! (" + fn_groups + "), ошибка: " + ex.getMessage());
        }
        catch(IOException ex) {
            System.out.println("Ошибка чтения из файла групп " + fn_groups +": " + ex.getMessage());
        }
        catch(ParseException ex) {
            System.out.println("Ошибка парсера: " + ex.getMessage());
        }

        // грузим студентов
        File fs = new File(fn_students);
        try {
            FileReader fr = new FileReader(fs);
            JSONParser parser = new JSONParser();
            JSONObject js = (JSONObject)parser.parse(fr);
            JSONArray items = (JSONArray)js.get("students"); // парсер разберет массив students
            int j = 1;
            for(Object i:items) {
                JSONObject k = (JSONObject)i;
                Student s = new Student(j++, k.get("name").toString()); // создадим студента по имени плюс счетчик для ID
                Group g = searchGroup(k.get("group").toString()); // поищем группу
                if(g!=null) {
                    s.setGroup(g); // если группа нашлась, назначим в нее
                    g.addStudent(s); // создадим перекрестную ссылку в группе на студента
                }
                try {
                    JSONArray marks = (JSONArray) k.get("marks"); // получить список оценок
                    if(!marks.isEmpty()) { // найден список оценок, но он пуст - ничего не делаем
                        for (Object m : marks) {
                            s.addMark(Integer.parseInt(m.toString()));
                        }
                    }
                }
                catch(NullPointerException ex) {
                    // не найден список оценок - ничего не делаем
                }
                students.add(s); // добавим в массив
            }
        }
        catch(FileNotFoundException ex) {
            System.out.println("Файл со студентами не найден! (" + fn_groups + "), ошибка: " + ex.getMessage());
        }
        catch(IOException ex) {
            System.out.println("Ошибка чтения из файла студентов (" + fn_students + "): " + ex.getMessage());
        }
        catch(ParseException ex) {
            System.out.println("Ошибка парсера: " + ex.getMessage());
        }
    }

    public ArrayList getStudents() { // получить список студентов
        return (ArrayList)students;
    }

    public ArrayList getGroups() { // получить список групп
        return (ArrayList)groups;
    }

    public Group searchGroup(String name) { // поиск группы по названию
        for(Group g:groups) {
            if(g.title.equals(name))
                return g;
        }
        return null;
    }

    public void getStudentList() { // вывести список студентов
        for(Student s:students) {
            System.out.println(s.getFIO() + ", " + s.getFIO());
        }
        System.out.println("Всего студентов: " + students.size());
    }

    public void getGroupList() { // вывести список групп
        int i=0;
        NumberFormat formatter = new DecimalFormat("#0.00");
        for(Group g:groups) {
            String headfio;
            if(g.getHead()==null) headfio="не выбран";
            else headfio=g.getHead().getFIO();
            System.out.println("Группа: " + g.title + ", студентов: " + g.groupSize() + ", ср.балл: " + formatter.format(g.avgGroupMark()) +
            ", староста: " + headfio);
            i+=g.groupSize();
        }
        System.out.println("Всего групп: " + groups.size() + ", студентов в группах: " + i);
    }

    public void getStatistics(HashMap<String, Double> oldgroups) { // получить статистику успеваемости по сравнению с предыдущим вектором оценок в группах
        for(String gs:oldgroups.keySet()) {
            Double g=oldgroups.get(gs);
            NumberFormat formatter = new DecimalFormat("#0.00");
            Group g1 = searchGroup(gs);
            Double g0 = g1.avgGroupMark();
            double progress=1-(g/g0);
            if(progress<=0) {
                System.out.println("Успеваемость упала на " + formatter.format(-progress) + "%");
            }
            else {
                System.out.println("Успеваемость повысилась на " + formatter.format(progress) + "%");
            }
        }
    }

    public void removeStudents(double v) { // отчислить студентов с оценкой меньше v
        int stcount=0;
        while(stcount<students.size()) {
            Student s=students.get(stcount);
            Double mark = s.getAvgMark();
            if (mark < v) {
                s.getGroup().removeStudent(s); // отчислить студента из группы
                students.remove(s);
            }
            else stcount++;
        }
    }

    public void moveToGroup(Student s, Group gn) { // переместить студента из группы
        s.getGroup().removeStudent(s);
        gn.addStudent(s);
        System.out.println("Студент " + s.getFIO() + " зачислен в группу "+ gn.title);
    }

    public void saveStudents(String fn_new) { // сохранить список студентов в файл
        File fs = new File(fn_new);
        try {
            FileWriter fr = new FileWriter(fs);
            JSONObject obj = new JSONObject();
            obj.put("name", "Список студентов"); // строчка-идентификатор

            JSONArray list = new JSONArray();
            for(Student s:students) {
                JSONObject stud = new JSONObject();
                stud.put("name", s.getFIO()); // ФИО
                stud.put("group", s.getGroup().title); // имя группы

                JSONArray marklist = new JSONArray(); // список оценок
                for(int i=0;i<s.getMarks().size();i++){
                    marklist.add(s.getMarks().get(i));
                }
                stud.put("marks", marklist); // поместить оценки в объект

                list.add(stud); // поместить студента в список
            }

            obj.put("students", list); // поместить список студентов в объект

            fr.write(obj.toJSONString()); // сохранить текстовый файлик json
            fr.flush();
            System.out.println("Список студентов сохранен в файле " + fn_new);
        }
        catch(IOException ex) {
            System.out.println("Ошибка IOException: " + ex.getMessage());
        }

    }
}
