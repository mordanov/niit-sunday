package netclient;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    private String address;
    private int port;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    private int id = 0;

    @FXML TextField serverAddr;
    @FXML TextField serverPort;

    @FXML Button btn1choice;
    @FXML TextField out1;
    @FXML Button btn2choice;
    @FXML TextArea out2;
    @FXML ComboBox comboIDOrg;
    @FXML TextArea textOrg;
    @FXML TextField valValue;
    @FXML ComboBox valChoice;
    @FXML TextField valResult;
    @FXML Button btngetVal;
    @FXML ListView log;

    private void commonButtonAction(String ActionText) {
        String info;
        try {
            out.println(ActionText);
        }
        catch(RuntimeException ex) {
            outputer(ex.getMessage());
        }

        try {
            info = in.readLine();
            parseInStr(info);
        }
        catch(IOException ex) {
            outputer(ex.getMessage());
        }
    }

    public BufferedReader getIn() {
        return in;
    }

    public PrintWriter getOut() {
        return out;
    }

    @FXML protected void btn1choiceaction(ActionEvent e) {
        commonButtonAction("getTime");
    }

    @FXML protected void btn2choiceaction(ActionEvent e) {
        commonButtonAction("getQuote");
    }

    @FXML protected void btngetValAction(ActionEvent e) {
        commonButtonAction("getCurs:" + valValue.getText() + ":" + valChoice.getValue().toString());
    }

    @FXML protected void comboIDOrgAction(ActionEvent e) {
        commonButtonAction("User_ID:" + comboIDOrg.getValue());
    }

    @Override @FXML
    public void initialize(URL url, ResourceBundle rb) {
        // приготовления перед запуском
        serverAddr.setText("127.0.0.1");
        serverPort.setText("6667");

        for(int i=0;i<10;i++) {
            comboIDOrg.getItems().add(i, String.valueOf(i));
        }
        valChoice.getItems().addAll(Arrays.asList("USD", "EUR", "JPY", "GBP", "AUD", "BYN", "BRL", "CAD", "SGD", "UAH", "CZK"));

        comboIDOrg.getSelectionModel().select(0);
        valChoice.getSelectionModel().select(0);
        valValue.setText("1.0");

        // создаем клиента

        address = serverAddr.getText();
        port = Integer.parseInt(serverPort.getText());

        try {
            socket = new Socket(address, port);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            outputer("Подключились к серверу: " + address + ":" + port);
            new Reminder(this).start();
        }
        catch(IOException ex) {
            outputer(ex.getMessage());
        }
    }

    private void outputer(String text) {
        Date ld = new Date();
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        log.getItems().add(0, df.format(ld) + ' ' + text);
    }

    void closewindow() {
        outputer("Закрываем сокет");
        try {
            socket.close();
        }
        catch(IOException | NullPointerException ex) {
            System.out.println(ex.getMessage());
            System.exit(1);
        }
    }

    void parseInStr(String str) {
        // формат сообщения - ID|header|сообщение
        final String rhead = "##reminds##";
        final String chead = "##getcurs##";
        final String thead = "##gettime##";
        final String qhead = "##getquot##";
        final String ihead = "##user_id##";
        final String separator = "&";

        String[] words = str.split(separator);

        if(this.id==Integer.parseInt(words[0])) // проверим ID
            switch(words[1]) {
                case rhead: // напоминание
                    textOrg.insertText(0, words[2]);
                    break;
                case chead: // курс валюты
                    valResult.setText(words[2]);
                    outputer("Получен курс валюты: " + words[2]);
                    break;
                case thead: // время
                    out1.setText(words[2]);
                    outputer("Получено текущее время: " + words[2]);
                    break;
                case qhead: // цитата
                    out2.setText(words[2]);
                    outputer("Получено цитата какого-то великого человека!");
                    break;
                case ihead: // ID
                    outputer("Новое ID = " + words[2]);
                    this.id = Integer.parseInt(words[2]);
                    break;
            }
    }

}
