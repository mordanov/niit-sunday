package netclient;

import java.io.IOException;

public class Reminder extends Thread {

    private Controller controller;

    Reminder(Controller controller) {
        this.controller = controller;
    }

    public void run() {
        String info = "";
        try {
            while (true) {
                controller.getOut().println("reminds");
                try {
                    info = controller.getIn().readLine();
                }
                catch (IOException ex) {
                    System.out.println(ex.getMessage());
                    return;
                }
                if(info.length()>0) {
                    controller.parseInStr(info);
                }
                sleep(1000);
            }
        }
        catch(InterruptedException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
