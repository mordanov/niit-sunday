package my.templates;

import java.util.ArrayList;
import java.util.List;

public class CoffeeDrink { // описание класса "напиток" - сам напиток плюс добавки
    private class CoffeeAdds { // описание класса "добавка"
        public int add = -1;
        public int count = 0;
        private int maxadd = 1;
        private long price = 0;

        public CoffeeAdds(int add, long price, int maxadd) { // добавить добавку
            this.add = add;
            this.count = 1;
            this.price = price;
            this.maxadd = maxadd;
        }

        public boolean modifyCount(int mod) { // изменить количество
            if(count+mod<=maxadd){
                count +=mod;
                return true;
            } else return false;

        }

        public long getPrice() { // общая цена
            return count*price;
        }
    }

    public String name;
    private int drink = -1;
    private boolean canAdd;
    private long price;
    private List<CoffeeAdds> adds = new ArrayList<CoffeeAdds>();

    public void setDrink(int drink, boolean canAdd, long price, String name) { // новый напиток
        this.drink = drink;
        this.canAdd = canAdd;
        this.price = price;
        this.name = name;
    }

    public int setAdd(int add, int count, long price, int maxadd) { // добавка
        if(!canAdd) {
            return -1;
        }
        else {
            for(int i=0;i<adds.size();i++) {
                if(adds.get(i).add == add) {
                    if(adds.get(i).modifyCount(count)) return 0;
                    else return 1;
                }
            }
            // если ничего не нашли
            adds.add(new CoffeeAdds(add, price, maxadd));
            return 2;
        }
    }

    public long getPrice() { // полная стоимость напитка с добавками
        long sum = price;
        for(int i=0;i<adds.size();i++) sum += adds.get(i).getPrice();
        return sum;
    }

    public void delDrink() { // очистить, перед новым заказом
        drink = -1;
        adds.clear();
    }

    public int getDrink() { // получить напиток
        return drink;
    }

    public int getAddsCount() { // сколько добавок в напитке
        return adds.size();
    }

}
