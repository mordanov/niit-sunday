package my;

import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import my.except.CoffeeException;
import my.templates.AbstractCoffeeOutputer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CoffeeOutputer implements AbstractCoffeeOutputer {

    private ListView log;
    private TextArea useroutput;

    public CoffeeOutputer(ListView listview, TextArea textarea) throws CoffeeException {
        this.log = listview;
        this.useroutput = textarea;
    }

    public void write2log(String message){
        Date curdate = new Date();
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        log.getItems().add(0, df.format(curdate).toString() + " " + message);
    }

    public void showLine(String message) {
        useroutput.appendText(message + System.getProperty("line.separator"));
    }

    public void stop() {};

    public void clearOutput() {
        useroutput.clear();
    }

}
